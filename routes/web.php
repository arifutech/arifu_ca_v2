<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/automate','AutomateController@index');
Route::post('/automate','AutomateController@store');

Route::get('/diagram','AutomateController@diagram');

Route::post('/partner','AutomateController@createPartner');
Route::post('/program','AutomateController@createProgram');