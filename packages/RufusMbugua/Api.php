<?php

namespace Packages\RufusMbugua;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
class Api{

	function __construct(){
		
	}

	static function get(String $resource='',Array $payload=[]){

		try {
			$client = new Client(['base_uri'=>env('API_URL')]);
			$response = $client->request($resource);

			return $response->getBody();
		} catch (RequestException $e) {
			dd($e->getMessage());
		}
		
	}
}