<?php

namespace App\Http\Controllers;

use App\Partner;
use App\Project;
use Excel;
use Illuminate\Http\Request;

class AutomateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(API::get('learners'));
        return view('automate.show');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Excel::store('ContentAutomationTemplate', function($excel) {

        //     $excel->sheet('Information', function($sheet) {

        //         $sheet->loadView('excel.information');
        //     });
        //     $excel->sheet('Menu', function($sheet) {

        //         $sheet->loadView('excel.menu');
        //     });
        //     $excel->sheet('Content', function($sheet) {

        //         $sheet->loadView('excel.content');
        //     });
        // })->download();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('excel')) {
            $reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
            $spreadsheet = $reader->load($request->excel);

            dd(self::handleWorkBook($request,$reader,$spreadsheet));
            // dd($reader->listWorksheetNames($request->excel));
            

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function diagram()
    {
        $projects = Project::all();
        $partners = Partner::all();
        return view('automate.diagram', compact('projects', 'partners'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createPartner(Request $request)
    {
        $data = $request->all();
    }

    public function createProgram(Request $request)
    {
        $data = $request->all();
    }

    public function handleWorkBook($request, $reader,$spreadsheet){
        $sheets = $reader->listWorksheetNames($request->excel);
        $workbook = [];
        foreach($sheets as $worksheet){
            switch($worksheet){
                case 'Information':
                   $data = self::handleInformationData($spreadsheet, $worksheet);
                break;

                default:
                    $data = self::handleContentData($spreadsheet, $worksheet);
                break;

            }
            $workbook[$worksheet] = $data;
        }

        return $workbook;
    }
    public function handleSheet($spreadsheet, $sheetName)
    {
        return $spreadsheet->getSheetByName($sheetName);
    }

    public function handleInformationData($spreadsheet, $sheetName)
    {
        $data      = [];
        $worksheet = self::handleSheet($spreadsheet, $sheetName);

        $worksheetData = [];
        // Loop through worksheet
        foreach ($worksheet->getRowIterator() as $row) {

            $cellIterator = $row->getCellIterator();
            $rowData      = [];
            // var_dump($row->getRowIndex());
            switch ($row->getRowIndex()) {
                default:
                    $cellIterator->setIterateOnlyExistingCells(false);
                    $cellIndex = 0;
                    $name      = '';
                    $value     = '';
                    foreach ($cellIterator as $cell) {
                        switch ($cellIndex) {
                            case 0:
                                $name = $cell->getValue();
                                break;

                            case 1:
                                $value = $cell->getValue();
                                break;

                        }
                        // dd($value);
                        $rowData[$name] = $value;
                        $cellIndex++;
                    }

                    break;
            }
            foreach ($rowData as $key => $value) {
                $worksheetData[$key] = $value;
            }

        }
        return collect($worksheetData);
    }

    public function handleContentData($spreadsheet, $sheetName)
    {
        $worksheet = self::handleSheet($spreadsheet, $sheetName);

        $worksheetData = [];
        // Loop through worksheet
        foreach ($worksheet->getRowIterator() as $row) {

            $cellIterator = $row->getCellIterator();
            $rowData      = [];
            // var_dump($row->getRowIndex());
            switch ($row->getRowIndex()) {
                case 1:
                    $titles = [];
                    foreach ($cellIterator as $cell) {
                        array_push($titles, strtolower($cell->getValue()));
                    }

                    break;
                default:
                    $cellIterator->setIterateOnlyExistingCells(false);
                    $cellIndex = 0;
                    foreach ($cellIterator as $cell) {
                        $rowData[$titles[$cellIndex]] = $cell->getValue();
                        $cellIndex++;
                    }

                    break;
            }
            if(!empty($rowData)){
                $worksheetData[] = $rowData;
            }
            
        }
        return collect($worksheetData);
    }

    function handleLinkedCell(){
        if(strpos($cell, '!')!==false){
            explode($cell,'!');
        }
    }

}
