<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
     function project(){
    	return $this->belongsTo('App\Project');
    }
}
