@extends('layouts.master')

@section('content')
	{{ Form::open(['url'=>'/automate','method'=>'post','files'=>true]) }}
		<h1>Content Automation</h1>
		<div class="alert alert-info">
			<i class="fa fa-info"></i> With this form, you can upload an excel file from your computer or from Google Drive
		</div>
		<input type="file" class="file" name="excel" style="display:none">
		<h2 style="display:none"></h2>
		<button type="submit" class="btn btn-primary excel"><i class="fas fa-file-excel"></i>Excel File</button>
		<button class="btn btn-primary drive"><i class="fab fa-google-drive"></i>Google Drive</button>
	{{ Form::close() }}
	<script>
		$('.excel').click(function(e){
			e.preventDefault();
			$('input.file').trigger('click')
		})

		$('input.file').change(function(){
			alert('yo')
			$('.excel').closest('form').submit();
		})
	</script>
@endsection