@extends('layouts.master')

@section('content')
	<div>
		
		<div class="dropdown">
		  <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    New
		  </button>
		  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    <a class="dropdown-item" href="#" id="partnerModalLink">Partner</a>
		    <a class="dropdown-item" href="#" id="projectModalLink">Project</a>
		    <a class="dropdown-item" href="#" id="programModalLink">Program</a>
		    <a class="dropdown-item" href="#">Training</a>
		  </div>
		</div>
	</div>
	@include('automate.modals')
	<script>
		$('#partnerModalLink').on('click',function(e){
			e.preventDefault();
			$('#partnerModal').modal('show')
		})

		$('#programModalLink').on('click',function(e){
			e.preventDefault();
			$('#programModal').modal('show')
		})

		$('#projectModalLink').on('click',function(e){
			e.preventDefault();
			$('#projectModal').modal('show')
		})
	</script>
@endsection