<div class="modal" tabindex="-1" role="dialog" id="partnerModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create a Partner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(['url'=>'/partner','method'=>'post','files'=>true]) }}
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control">
           
          </div>
          <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
          </div>
        {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- Project --}}
<div class="modal" tabindex="-1" role="dialog" id="projectModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create a Project</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(['url'=>'/project','method'=>'post','files'=>true]) }}
        <div class="form-group">
            <label for="">Partner</label>
            <select name="" id="" class="form-control">
              <option value="">Select one</option>
                @foreach($partners as $partner)
                  <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control">  
          </div>
          
        {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

{{-- Program --}}

<div class="modal" tabindex="-1" role="dialog" id="programModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create a Program</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ Form::open(['url'=>'/program','method'=>'post','files'=>true]) }}
        <div class="form-group">
            <label for="">Partner</label>
            <select name="" id="" class="form-control">
              <option value="">Select one</option>
                @foreach($projects as $project)
                  <option value="{{ $project->id }}">{{ $project->project_name }}</option>
                @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control">  
          </div>
          
        {{ Form::close() }}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>